#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>
#include <QObject>

#include "gamelogic.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

	QQmlApplicationEngine   engine;
	GameLogic               logic;

	engine.rootContext()->setContextProperty("logic", &logic);
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
