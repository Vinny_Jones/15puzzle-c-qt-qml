import QtQuick 2.6

Rectangle {
//    anchors.top: parent.top
//    anchors.topMargin: 5
//    anchors.left: parent.left
//    anchors.leftMargin: 5
//    width: parent.width - 10
//    height: parent.height - 10

	anchors.margins: 5
	anchors.fill: parent
    radius: 10

	property string label: ""

	Text {
        anchors.centerIn: parent
        text: label
		font.pixelSize: parent.width * 0.7
    }
}
