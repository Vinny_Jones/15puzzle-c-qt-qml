import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Window 2.2

Window {
    id: mainWinow
    visible: true
    width: 600
    height: 700
    title: qsTr("П'ятнашки")

    property int fieldSide: logic.fieldSide
    property int totalElements: logic.totalElements
    property int menuHeight: 100
    property int pieceSide: width / fieldSide

    /* Only width is allowed to change so that game piece always remain square */
    onWidthChanged:
    {
        height = width + menuHeight;
        pieceSide = width / fieldSide;
//        for (var i = 0; i < totalElements; i++)
//        {
//            gamePieces.itemAt(i).updatePosition();
//        }
    }
    onHeightChanged: height = width + menuHeight
    minimumWidth: 400
    maximumWidth: 1000

	Topmenu {
		id:menu
		anchors.top: parent.top
		anchors.left: parent.left
		width: parent.width
		height: menuHeight
	}

	Rectangle {
		id:gameFeild
		anchors.top: menu.bottom
		anchors.left: parent.left
		width: parent.width
		height: width
		color: "#EAAD79"

		ListModel {
			id:gameModel

			Component.onCompleted: {
				for (var i = 0; i < logic.totalElements; i++)
					gameModel.append({ label: i })
			}
		}

		GridView {
			id: gamePieces
			anchors.fill: parent
			model:gameModel
			cellWidth: pieceSide
			cellHeight: cellWidth

			delegate: Item {
				id:gamePiece
				width: pieceSide
				height: pieceSide

				property int currentLabel: logic.getLabel(model.label)

				PuzzleItem {
					color: (currentLabel) ? "lightblue" : "transparent"
					label: currentLabel ? currentLabel : ""

					MouseArea {
						anchors.fill: parent
						onReleased: logic.makeMove(gamePiece.currentLabel, index)
					}
				}
			}

			move: Transition {
				NumberAnimation { properties: "x,y"; duration: 250 }
			}
		}

//		Repeater {
//			id: gamePieces
//			model: totalElements

//			Rectangle {
//				id:gamePiece
//				property int piecePosition: parseInt(logic.elemsPosition[index])
//				color: "transparent"
//				width: pieceSide
//				height: pieceSide

//				PuzzleItem {
//					color: (index) ? "lightblue" : "transparent"
//					label: (index) ? index : ""
//				}
//				MouseArea {
//					anchors.fill: parent
//					onReleased: logic.makeMove(index, gamePiece.piecePosition)
//				}

//				Behavior on x { NumberAnimation { duration: 250 } }
//				Behavior on y { NumberAnimation { duration: 250 } }

//				Component.onCompleted: updatePosition()

//				function updatePosition()
//				{
//					piecePosition = parseInt(logic.elemsPosition[index]);
//					x = (piecePosition % fieldSide) * pieceSide;
//					y = parseInt(piecePosition / fieldSide) * pieceSide;
//				}
//			}
//		}
	}

    Rectangle {
        id:gameFinished
        anchors.fill: gameFeild
        z:1
        color: Qt.rgba(1,1,1,0.8)
        visible: false

        MouseArea { anchors.fill: parent }

        Rectangle{
            anchors.centerIn: parent
            width: parent.width / 2
            height: width / 3
            color: "lightblue"
            radius: 10

            Text {
                text: qsTr("You won :) Congratulations")
                font.pixelSize: 20
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 20
            }

            CustomButton {
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 15
                anchors.horizontalCenter: parent.horizontalCenter
                width: 100
                height: 30
                label: "OK"
                function buttonClick()
                {
                    gameFinished.visible = false;
					logic.shuffle();
                }
            }
        }
    }

    Connections {
        target: logic
        onFinishSignal: gameFinished.visible = true
		onElementChanged:
		{
			if (nullPos - 1 == pos)
			{
				gameModel.move(pos, nullPos, 1);
//				console.log("here")
			}
			else if (pos > nullPos)
			{
				gameModel.move(pos, nullPos, 1);
				gameModel.move(nullPos + 1, pos, 1);
//				console.log("or here")
			}
			else
			{
				gameModel.move(nullPos, pos, 1);
				gameModel.move(pos + 1, nullPos, 1);
//				console.log("or here 2")
			}
		}
    }
}
