import QtQuick 2.6
import QtQuick.Dialogs 1.2

Rectangle {
    color: "#FEE6E6"
    property int buttonWidth: 170
	property int buttonHeight: height - 20
    property int spacing: (parent.width - buttonWidth * 2) / 3

    CustomButton {
        id:shuffleButton
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: spacing
        width: buttonWidth
        height: buttonHeight

        label: "Shuffle"
        function buttonClick()
        {
            logic.shuffle()
        }

    }

    CustomButton {
        id:exitButton
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.left: shuffleButton.right
        anchors.leftMargin: spacing
        width: buttonWidth
        height: buttonHeight

        label: "Exit"
        function buttonClick()
        {
            dialogOnExit.open()
        }
        MessageDialog {
            id:dialogOnExit
            title: "Exiting..."
            icon: StandardIcon.Question
            text: "Exit game?"
            standardButtons: StandardButton.Yes | StandardButton.No
            onYes: Qt.quit()
        }
    }
}
