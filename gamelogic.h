#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include <QObject>
#include <QDebug>

#define TOTAL_ELEMENTS 16
#define FIELD_SIDE 4
#define SHUFFLE_MOVES (TOTAL_ELEMENTS * TOTAL_ELEMENTS)
#define ROLE 0
#define VALID_INDEX(X) ((X) >= 0 && (X) < TOTAL_ELEMENTS)

class GameLogic : public QObject
{
    Q_OBJECT
	Q_PROPERTY(int fieldSide READ getFieldSide() CONSTANT)
	Q_PROPERTY(int totalElements READ getElementsAmount() CONSTANT)
//	Q_PROPERTY(QStringList elemsPosition READ getElemsPosition() CONSTANT)

public:
	GameLogic();
    ~GameLogic();
	int                     getFieldSide() const;
	int                     getElementsAmount() const;
//	QStringList             getElemsPosition();

signals:
	void                    finishSignal();
//	void                    elementChanged(int index);
	void					elementChanged(int pos, int nullPos);

public slots:
	void                    makeMove(int index, int position);
	void                    shuffle();
	int						getLabel(int index);

private:
    bool                    finishedGame() const;
	QStringList             elementsPosition;
};

#endif // GAMELOGIC_H
