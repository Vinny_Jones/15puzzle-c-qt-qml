import QtQuick 2.6
import QtQuick.Controls 1.3

Rectangle {
    border.color: "white"
    border.width: 3
    radius: 10

    property string label: "Button"

    Text
    {
        text: qsTr(label)
        font.pixelSize: parent.height * 0.4
        anchors.centerIn: parent
        color: "black"
        font.bold: true
    }

    property color buttonColor: "lightblue"
    property color onHoverColor: "gold"
    property color borderColor: "white"

    MouseArea {
        id: buttonMouseArea
        anchors.fill: parent
        onClicked: buttonClick()
        hoverEnabled: true
        onEntered: parent.border.color = parent.onHoverColor
        onExited: parent.border.color = parent.borderColor
    }

    function buttonClick() {}

    color: buttonMouseArea.pressed ? "#ffce9e" : buttonColor
    Behavior on color { ColorAnimation { duration: 100 } }

    scale: buttonMouseArea.pressed ? 1.1 : 1.0
    Behavior on scale { NumberAnimation{ duration: 100 } }
}

