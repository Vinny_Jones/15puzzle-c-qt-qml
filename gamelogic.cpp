#include "gamelogic.h"

/* Comment shuffle() it to see correct positions (before shuffling) */
GameLogic::GameLogic()
{
	elementsPosition << QString::number(TOTAL_ELEMENTS - 1);
	for (int i = 0; i < TOTAL_ELEMENTS - 1; i++)
	{
		elementsPosition << QString::number(i);
	}
	shuffle();
}

GameLogic::~GameLogic() {
}

int     GameLogic::getFieldSide() const
{
	return (FIELD_SIDE);
}

int     GameLogic::getElementsAmount() const
{
	return (TOTAL_ELEMENTS);
}


int		GameLogic::getLabel(int index)
{
	for (int i = 0; i < elementsPosition.size(); ++i)
	{
		if (elementsPosition[i].toInt() == index)
			return (i);
	}
	return (-1);
}


//QStringList GameLogic::getElemsPosition()
//{
//	return elementsPosition;
//}

void    GameLogic::makeMove(int numLabel, int position)
{
	int nullPosition = elementsPosition[0].toInt();

	if (!VALID_INDEX(numLabel) || !VALID_INDEX(position))     /* This should never happen */
		return ;
//	qInfo() << numLabel << position << nullPosition;
    switch (position - nullPosition)
    {
    case (1):
        if (!(position % 4))
        {
            return ;
        }
        break ;
    case (-1):
        if (!(nullPosition % 4))
        {
            return ;
        }
        break ;
	case (FIELD_SIDE):
	case (-FIELD_SIDE):
        break ;
    default:
        return ;
    }
	elementsPosition.swap(numLabel, 0);
//	emit elementChanged(0);
//	emit elementChanged(numLabel);

	emit elementChanged(position, nullPosition);
    if (finishedGame())
        emit finishSignal();
}

void    GameLogic::shuffle()
{
    int     direction[] = {1, -1, 4, -4};
    int     position;

//	this->blockSignals(true);
	srand(time(NULL));
	for (int i = 0; i < SHUFFLE_MOVES; i++)
	{
		position = elementsPosition[0].toInt() + direction[rand() % 4];
		if (!(VALID_INDEX(position)))
			continue ;
		makeMove(getLabel(position), position);
	}
//	this->blockSignals(false);
//	for (int i = 0; i < TOTAL_ELEMENTS; i++)
//		emit elementChanged(i);
}

bool    GameLogic::finishedGame() const
{
	for (int i = 1; i < TOTAL_ELEMENTS; i++)
    {
		if (elementsPosition[i].toInt() != i - 1)
            return (false);
    }
    return (true);
}
